#pragma once
#include <vector>
#include <d3dx9.h>


void GenTriGrid(int numVertRows, int numVertCols,
				float dx, float dz,
				const D3DXVECTOR3& center,
				std::vector<D3DXVECTOR3>& verts,
				std::vector<DWORD>& indices);

