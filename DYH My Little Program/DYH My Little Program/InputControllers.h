#pragma once
#include <Windows.h>
class KeyboardController
{
public:
	void HandleKeyDown(WPARAM wParam);
	void HandleKeyUp(WPARAM wParam);
	
	//Keys
	bool space;
	bool esc;
	bool a;
	bool d;
	bool f;
	bool s;
	bool w;
};

class MouseController
{
public:
	MouseController(IDirectInput8 *di, DWORD mouseCoopFlags);
	~MouseController();

	void poll();
	bool leftdown();
	bool middledown();
	bool rightdown();
	float dx();
	float dy();
	float dz();

private:
	// Make private to prevent copying of members of this class.
	MouseController(const MouseController& rhs);
	MouseController& operator=(const MouseController& rhs);

	IDirectInput8 *mdinput;
	IDirectInputDevice8 *mMouse;
	DIMOUSESTATE2 mMouseState;
};

// TODO:
class PadController
{

};