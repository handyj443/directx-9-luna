// Does basic ambient, diffuse, and specular lighting.
//=============================================================================


uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInverseTranspose;
uniform extern float4x4 gWVP;

uniform extern float3 gLightVecW;
uniform extern float3 gEyePosW;

uniform extern float4 gDiffuseMtrl;
uniform extern float4 gDiffuseLight;
uniform extern float4 gAmbientMtrl;
uniform extern float4 gAmbientLight;
uniform extern float4 gSpecularMtrl;
uniform extern float4 gSpecularLight;
uniform extern float gSpecularPower;

uniform extern float gTime;

struct OutputVS
{
	float4 posH : POSITION0;
	float3 posW : TEXCOORD0;
	float3 normalW : TEXCOORD1;
	float4 color : COLOR0;
};

OutputVS AmbientDiffuseSpecVS(float3 posL : POSITION0, float3 normalL: NORMAL0)
{
	OutputVS outVS = (OutputVS)0;
	
	// Transform vertex to clip space and world space
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	outVS.posW = mul(float4(posL, 1.0f), gWorld).xyz;

	// Transform normal to world space
	float3 normalW = mul(float4(normalL, 0.0f), gWorldInverseTranspose).xyz;

	outVS.normalW = normalW;

	//_____________________________________________________________
	// Computer the color
	// Ambient
	float4 ambient = gAmbientMtrl * gAmbientLight;

	// Diffuse
	float4 diffuse = gDiffuseMtrl * gDiffuseLight * dot(normalW, gLightVecW);

	outVS.color.rgb = (ambient + diffuse).xyz;
	outVS.color.a = gDiffuseMtrl.a;

	return outVS;
}

float4 AmbientDiffuseSpecPS(float3 posW : TEXCOORD0, 
							float3 normalW : TEXCOORD1, 
							float4 color : COLOR0) : COLOR0
{
	// Specular
	normalW = normalize(normalW);
	float3 vertexToEye = normalize(gEyePosW - posW);
	float3 halfAngle = normalize(vertexToEye + gLightVecW);
	float4 specular = gSpecularMtrl * gSpecularLight *
		pow(max(dot(normalW, halfAngle), 0), gSpecularPower);
	color += specular;

	return color;
}

technique AmbientDiffuseSpecTech
{
	pass POSITION0
	{
		vertexShader = compile vs_2_0 AmbientDiffuseSpecVS();
        pixelShader  = compile ps_2_0 AmbientDiffuseSpecPS();
	}
}