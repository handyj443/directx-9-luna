// An attempt to code a procedural DirectX 9 program using 
// Luna's book as a guide.

#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include <tchar.h>
#include <string>
#include <vector>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include "InputControllers.h"
#include "geometry.h"
#include "GfxStats.h"


using std::string;
using std::vector;
#if defined(DEBUG) | defined(_DEBUG)
	#define D3D_DEBUG_INFO
#endif


//______________________________________________________________________________
// MACROS
#include "macros.h"

//______________________________________________________________________________
// FORWARD DECLARATIONS
void enableFullScreenMode(bool enable);
void onResetDevice();
void onLostDevice();

//______________________________________________________________________________
// VERTEX DEFINITIONS
struct VertexCol
{
	D3DXVECTOR3 pos;
	D3DCOLOR col;
};
struct VertexPosNormal
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
};
struct VertexPosNormalUV
{
	VertexPosNormalUV(float x, float y, float z,
			  float nx, float ny, float nz,
			  float u, float v) :pos(x, y, z), normal(nx, ny, nz), tex0(u, v) {}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 normal;
	D3DXVECTOR2 tex0;
};
struct VertexPosOffsetUV
{
	VertexPosOffsetUV(float x, float y, float z,
					  float nx, float ny, float nz,
					  float u, float v) :pos(x, y, z), offset(nx, ny, nz), tex0(u, v) {}
	D3DXVECTOR3 pos;
	D3DXVECTOR3 offset;
	D3DXVECTOR2 tex0;
};
D3DVERTEXELEMENT9 VertexColElements[] = { 
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, 
	   D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT,
	   D3DDECLUSAGE_COLOR, 0 },
	D3DDECL_END()
};
D3DVERTEXELEMENT9 VertexPosNormalElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_NORMAL, 0 },
	D3DDECL_END()
};
D3DVERTEXELEMENT9 VertexPosNormalUVElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_NORMAL, 0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 0 },
	D3DDECL_END()
};
D3DVERTEXELEMENT9 VertexPosOffsetUVElements[] = {
	{ 0, 0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_POSITION, 0 },
	{ 0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 0 },
	{ 0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT,
	D3DDECLUSAGE_TEXCOORD, 1 },
	D3DDECL_END()
};
IDirect3DVertexDeclaration9 *VertexColDecl = nullptr;
IDirect3DVertexDeclaration9 *VertexPosNormalDecl = nullptr;
IDirect3DVertexDeclaration9 *VertexPosNormalUVDecl = nullptr;
IDirect3DVertexDeclaration9 *VertexPosOffsetUVDecl = nullptr;

//______________________________________________________________________________
// GLOBALS

// Direct3D
IDirect3D9 *gd3d = nullptr;
IDirect3DDevice9 *gd3dDevice = nullptr;
D3DPRESENT_PARAMETERS gd3dPP = {};

// Input controllers
KeyboardController gKeyboard;
IDirectInput8* gdinput = nullptr;
MouseController* gMouse = nullptr;  // using DirectInput
PadController gPad;  // using xInput

// Camera
float gCameraRadius;
float gCameraRotationY;
float gCameraHeight;
D3DXMATRIX gViewMat;
D3DXMATRIX gProjMat;
D3DXMATRIX gWorldMat;

// Application control
HWND ghMainWnd = 0;
bool gAppCreated;
bool gAppPaused;
float gTime;
GfxStats *gstats;

// Geometry and material rendering
IDirect3DVertexBuffer9 *gGroundVB;
IDirect3DIndexBuffer9 *gGroundIB;
IDirect3DTexture9 *gGroundTex;
IDirect3DVertexBuffer9 *gTreeVB;
IDirect3DIndexBuffer9 *gTreeIB;
IDirect3DTexture9 *gTreeTex;

int gNumVertices;
int gNumTriangles;
ID3DXEffect *gfx;
D3DXVECTOR3 gLightVecW;
D3DXCOLOR gDiffuseMtrl;
D3DXCOLOR gDiffuseLight;
D3DXCOLOR gAmbientMtrl;
D3DXCOLOR gAmbientLight;
D3DXCOLOR gSpecularMtrl;
D3DXCOLOR gSpecularLight;
float gSpecularPower;

// Handles
D3DXHANDLE ghTime;
D3DXHANDLE ghGroundTech;
D3DXHANDLE ghTreeTech;
D3DXHANDLE ghWVP;
D3DXHANDLE ghViewProj;
D3DXHANDLE ghWorldInverseTranspose;
D3DXHANDLE ghLightVecW;
D3DXHANDLE ghDiffuseMtrl;
D3DXHANDLE ghDiffuseLight;
D3DXHANDLE ghAmbientMtrl;
D3DXHANDLE ghAmbientLight;
D3DXHANDLE ghSpecularMtrl;
D3DXHANDLE ghSpecularLight;
D3DXHANDLE ghSpecularPower;
D3DXHANDLE ghEyePos;
D3DXHANDLE ghWorld;
D3DXHANDLE ghGroundTex;
D3DXHANDLE ghTreeTex;

//______________________________________________________________________________
// APPLICATION SPECIFIC FUNCTIONS

void initializeApp() {
    gstats = new GfxStats();

	// Initial camera position
	gCameraRadius = 4.06f;
	gCameraRotationY = -1;
	gCameraHeight = 1.874f;
    //gCameraRadius = 6.0f;
    //gCameraRotationY = 1.2f * D3DX_PI;
    //gCameraHeight = 3.0f;
    
    gLightVecW = D3DXVECTOR3(0, 0, -1);
    
    gDiffuseMtrl   = D3DXCOLOR(1, 1, 1, 1);
    gDiffuseLight  = D3DXCOLOR(1.f, 1.f, 1.f, 1);
	gAmbientMtrl   = D3DXCOLOR(1, 1, 1, 1);
	gAmbientLight  = D3DXCOLOR(0.3f, 0.35f, 0.4f, 1.0f);
	gSpecularMtrl  = D3DXCOLOR(0.3f, 0.3f, 0.3f, 0.f);
	gSpecularLight = D3DXCOLOR(1, 1, 1, 1);
	gSpecularPower = 8;
	
	D3DXMatrixIdentity(&gWorldMat);
	HR(D3DXCreateTextureFromFile(gd3dDevice, "ground0.dds", &gGroundTex));
	HR(D3DXCreateTextureFromFile(gd3dDevice, "tree.dds", &gTreeTex));

	//_____________________________________________________________
	// Build ground geometry
	HR(gd3dDevice->CreateVertexBuffer(4 * sizeof(VertexPosNormalUV), D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &gGroundVB, 0));
	VertexPosNormalUV* v = 0;
	HR(gGroundVB->Lock(0, 0, (void**)&v, 0));
	v[0] = VertexPosNormalUV(-10.0f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 
							 5.0f, 5.0f);
	v[1] = VertexPosNormalUV(10.0f, 0.0f, 10.0f, 0.0f, 1.0f, 0.0f, 
							 0.0f, 0.0f);
	v[2] = VertexPosNormalUV(10.0f, 0.0f, -10.0f, 0.0f, 1.0f, 0.0f, 
							 0.0f, 5.0f);
	v[3] = VertexPosNormalUV(-10.0f, 0.0f, 10.0f, 0.0f, 1.0f, 0.0f, 
							 5.0f, 0.0f);
	HR(gGroundVB->Unlock());

	// Create the index buffer.
	HR(gd3dDevice->CreateIndexBuffer(6 * sizeof(WORD), D3DUSAGE_WRITEONLY,
							  D3DFMT_INDEX16, D3DPOOL_MANAGED, &gGroundIB, 0));
	WORD* i = 0;
	HR(gGroundIB->Lock(0, 0, (void**)&i, 0));
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 3; i[5] = 1;
	HR(gGroundIB->Unlock());


	// Build tree quads
#define NUM_TREES 4
	HR(gd3dDevice->CreateVertexBuffer(4 * NUM_TREES * sizeof(VertexPosOffsetUV), D3DUSAGE_WRITEONLY, 0, D3DPOOL_MANAGED, &gTreeVB, 0));
	VertexPosOffsetUV *w = 0;
	HR(gTreeVB->Lock(0, 0, (void**)&w, 0));
	w[0] = VertexPosOffsetUV(-1.0f, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f,
							 0.0f, 1.0f);
	w[1] = VertexPosOffsetUV(-1.0f, 2.0f, 0.0f, 5.0f, 0.0f, 0.0f,
							 0.0f, 0.0f);
	w[2] = VertexPosOffsetUV(1.0f, 2.0f, 0.0f, 5.0f, 0.0f, 0.0f,
							 1.0f, 0.0f);
	w[3] = VertexPosOffsetUV(1.0f, 0.0f, 0.0f, 5.0f, 0.0f, 0.0f,
							 1.0f, 1.0f);
	
	w[4] = VertexPosOffsetUV(-1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
							 0.0f, 1.0f);
	w[5] = VertexPosOffsetUV(-1.0f, 2.0f, 0.0f, 1.0f, 0.0f, 0.0f,
							 0.0f, 0.0f);
	w[6] = VertexPosOffsetUV(1.0f, 2.0f, 0.0f, 1.0f, 0.0f, 0.0f,
							 1.0f, 0.0f);
	w[7] = VertexPosOffsetUV(1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
							 1.0f, 1.0f);
	
	w[8] = VertexPosOffsetUV(-1.0f, 0.0f, 0.0f, -2.0f, 0.0f, 1.0f,
							 0.0f, 1.0f);
	w[9] = VertexPosOffsetUV(-1.0f, 2.0f, 0.0f, -2.0f, 0.0f, 1.0f,
							 0.0f, 0.0f);
	w[10] = VertexPosOffsetUV(1.0f, 2.0f, 0.0f, -2.0f, 0.0f, 1.0f,
							 1.0f, 0.0f);
	w[11] = VertexPosOffsetUV(1.0f, 0.0f, 0.0f, -2.0f, 0.0f, 1.0f,
							 1.0f, 1.0f);
	
	w[12] = VertexPosOffsetUV(-1.0f, 0.0f, 0.0f, -3.0f, 0.0f, 3.0f,
							 0.0f, 1.0f);
	w[13] = VertexPosOffsetUV(-1.0f, 2.0f, 0.0f, -3.0f, 0.0f, 3.0f,
							 0.0f, 0.0f);
	w[14] = VertexPosOffsetUV(1.0f, 2.0f, 0.0f, -3.0f, 0.0f, 3.0f,
							 1.0f, 0.0f);
	w[15] = VertexPosOffsetUV(1.0f, 0.0f, 0.0f, -3.0f, 0.0f, 3.0f,
							 1.0f, 1.0f);
	HR(gTreeVB->Unlock());

	// Create the index buffer.
	HR(gd3dDevice->CreateIndexBuffer(6 * NUM_TREES * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &gTreeIB, 0));
	i = 0;
	HR(gTreeIB->Lock(0, 0, (void**)&i, 0));
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;
	
	i[6] = 4; i[7] = 5; i[8] = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;
	
	i[12] = 8; i[13] = 9; i[14] = 10;
	i[15] = 8; i[16] = 10; i[17] = 11;
	
	i[18] = 12; i[19] = 13; i[20] = 14;
	i[21] = 12; i[22] = 14; i[23] = 15;
	HR(gTreeIB->Unlock());


	//_____________________________________________________________
	// Build fx
	ID3DXBuffer* errors = nullptr;

	HR(D3DXCreateEffectFromFile(gd3dDevice, "dirLightTex.fx", 0, 0,
								D3DXSHADER_DEBUG, 0, &gfx, &errors));
	if (errors) {
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);
	}

    // Obtain handles
	ghGroundTech = gfx->GetTechniqueByName("DirLightTexTech");
	ghTreeTech = gfx->GetTechniqueByName("TreeTech");
    ghTime = gfx->GetParameterByName(0, "gTime");
    ghWVP = gfx->GetParameterByName(0, "gWVP");
	ghViewProj = gfx->GetParameterByName(0, "gViewProj");
    ghWorldInverseTranspose =
        gfx->GetParameterByName(0, "gWorldInverseTranspose");
    ghLightVecW = gfx->GetParameterByName(0, "gLightVecW");
    ghDiffuseMtrl = gfx->GetParameterByName(0, "gDiffuseMtrl");
    ghDiffuseLight = gfx->GetParameterByName(0, "gDiffuseLight");
    ghAmbientMtrl = gfx->GetParameterByName(0, "gAmbientMtrl");
    ghAmbientLight = gfx->GetParameterByName(0, "gAmbientLight");
    ghSpecularMtrl = gfx->GetParameterByName(0, "gSpecularMtrl");
    ghSpecularLight = gfx->GetParameterByName(0, "gSpecularLight");
    ghSpecularPower = gfx->GetParameterByName(0, "gSpecularPower");
    ghEyePos = gfx->GetParameterByName(0, "gEyePosW");
    ghWorld = gfx->GetParameterByName(0, "gWorld");
	ghGroundTex = gfx->GetParameterByName(0, "gGroundTex");
	ghTreeTex = gfx->GetParameterByName(0, "gTreeTex");

	//_____________________________________________________________
	// Set the device state to initial conditions
	onResetDevice();

	// Initialise vertex declarations
	HR(gd3dDevice->CreateVertexDeclaration(VertexPosNormalUVElements, &VertexPosNormalUVDecl));
	HR(gd3dDevice->CreateVertexDeclaration(VertexPosOffsetUVElements, &VertexPosOffsetUVDecl));
}

void shutdownApp()
{
	delete gstats;
	ReleaseCOM(gfx);
	ReleaseCOM(gGroundVB);
	ReleaseCOM(gGroundIB);
	ReleaseCOM(gTreeVB);
	ReleaseCOM(gTreeIB);
	ReleaseCOM(gGroundTex);
	ReleaseCOM(gTreeTex);

	ReleaseCOM(VertexPosNormalUVDecl);
	ReleaseCOM(VertexPosOffsetUVDecl);

	ReleaseCOM(gdinput);
}

void updateScene(float dt)
{
	gMouse->poll();
	gstats->update(dt);
	gTime += dt;

	// Fullscreen toggle
	if (gKeyboard.esc)
		enableFullScreenMode(false);
	else if (gKeyboard.f)
		enableFullScreenMode(true);

	// Move global camera
	if (gKeyboard.w)
		gCameraHeight += 2.50f * dt;
	if (gKeyboard.s)
		gCameraHeight -= 2.50f * dt;

	gCameraRotationY += gMouse->dx() / 100.0f;
	gCameraRadius += gMouse->dy() / 25.0f;

	// If we rotate over 360 degrees, just roll back to 0
	if (fabsf(gCameraRotationY) >= 2.0f * D3DX_PI)
		gCameraRotationY = 0.0f;

	// Don't let radius get too small.
	if (gCameraRadius < 2.5f)
		gCameraRadius = 2.5f;

	// Build global view matrix
	float x = gCameraRadius * cosf(gCameraRotationY);
	float z = gCameraRadius * sinf(gCameraRotationY);
	D3DXVECTOR3 pos(x, gCameraHeight, z);
	D3DXVECTOR3 target(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&gViewMat, &pos, &target, &up);
	
	HR(gfx->SetValue(ghEyePos, &pos, sizeof(D3DXVECTOR3)));
}

void drawScene()
{
	HR(gd3dDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_XRGB(180, 222, 250), 1.0f, 0));
	
	//_____________________________________________________________
	// BEGIN
	HR(gd3dDevice->BeginScene());  
	HR(gd3dDevice->SetVertexDeclaration(VertexPosNormalUVDecl));
	HR(gd3dDevice->SetStreamSource(0, gGroundVB, 0, sizeof(VertexPosNormalUV)));
	HR(gd3dDevice->SetIndices(gGroundIB));

	//_____________________________________________________________
	// Pass values to the FX
	
	// GROUND
	HR(gfx->SetTechnique(ghGroundTech));

	D3DXMATRIX viewProj = gViewMat * gProjMat;
	D3DXMATRIX wvp = gWorldMat * viewProj;
	HR(gfx->SetMatrix(ghWVP, &wvp));

	D3DXMATRIX worldInverseTranspose;
	D3DXMatrixInverse(&worldInverseTranspose, nullptr, &gWorldMat);
	D3DXMatrixTranspose(&worldInverseTranspose, &worldInverseTranspose);

	HR(gfx->SetMatrix(ghWVP, &wvp));
	HR(gfx->SetMatrix(ghViewProj, &viewProj));
	HR(gfx->SetMatrix(ghWorldInverseTranspose, &worldInverseTranspose));
	
	HR(gfx->SetValue(ghLightVecW, &gLightVecW, sizeof(D3DXVECTOR3)));
	HR(gfx->SetValue(ghDiffuseMtrl, &gDiffuseMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghDiffuseLight, &gDiffuseLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghAmbientMtrl, &gAmbientMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghAmbientLight, &gAmbientLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghSpecularMtrl, &gSpecularMtrl, sizeof(D3DXCOLOR)));
	HR(gfx->SetValue(ghSpecularLight, &gSpecularLight, sizeof(D3DXCOLOR)));
	HR(gfx->SetFloat(ghSpecularPower, gSpecularPower));
	HR(gfx->SetMatrix(ghWorld, &gWorldMat));
	HR(gfx->SetTexture(ghGroundTex, gGroundTex));
	HR(gfx->SetTexture(ghTreeTex, gTreeTex));

	HR(gfx->SetFloat(ghTime, gTime));

	//_____________________________________________________________
	// Begin passes
	UINT numPasses;
	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2));
		HR(gfx->EndPass());
	}
	HR(gfx->End());

	// TREES
	HR(gd3dDevice->SetVertexDeclaration(VertexPosOffsetUVDecl));
	HR(gd3dDevice->SetStreamSource(0, gTreeVB, 0, sizeof(VertexPosOffsetUV)));
	HR(gd3dDevice->SetIndices(gTreeIB));

	HR(gfx->SetTechnique(ghTreeTech));

	HR(gfx->Begin(&numPasses, 0));
	for (UINT i = 0; i < numPasses; ++i) {
		HR(gfx->BeginPass(i));
		HR(gd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4 * NUM_TREES, 0, 2 * NUM_TREES));
		HR(gfx->EndPass());
	}
	HR(gfx->End());

	//_____________________________________________________________
	// END
	HR(gd3dDevice->EndScene());  

	gstats->display();
	HR(gd3dDevice->Present(0, 0, 0, 0));
}

void onLostDevice()
{
	HR(gfx->OnLostDevice());
	gstats->onLostDevice();
}

void onResetDevice()
{
	HR(gfx->OnResetDevice());
	gstats->onResetDevice();

	// Build global projection matrix
	float w = (float)gd3dPP.BackBufferWidth;
	float h = (float)gd3dPP.BackBufferHeight;
	D3DXMatrixPerspectiveFovLH(&gProjMat, D3DX_PI * 0.25f, w / h, 0.1f, 500.0f);

	//gd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
}

//_________________________________________________________________
// GENERAL FUNCTIONS
void enableFullScreenMode(bool enable)
{
	// Switch to fullscreen mode.
	if (enable)	{
		// Are we already in fullscreen mode?
		if (!gd3dPP.Windowed)
			return;

		int width = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		gd3dPP.BackBufferFormat = D3DFMT_X8R8G8B8;
		gd3dPP.BackBufferWidth = width;
		gd3dPP.BackBufferHeight = height;
		gd3dPP.Windowed = false;

		// Change the window style to a more fullscreen friendly style.
		SetWindowLongPtr(ghMainWnd, GWL_STYLE, WS_POPUP);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(ghMainWnd, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);
	}
	// Switch to windowed mode.
	else {
		// Are we already in windowed mode?
		if (gd3dPP.Windowed) {
			return;
		}

		RECT R = { 0, 0, 800, 600 };
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		gd3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
		gd3dPP.BackBufferWidth = 800;
		gd3dPP.BackBufferHeight = 600;
		gd3dPP.Windowed = true;

		// Change the window style to a more windowed friendly style.
		SetWindowLongPtr(ghMainWnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(ghMainWnd, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	// Reset the device with the changes.
	onLostDevice();
	HR(gd3dDevice->Reset(&gd3dPP));
	onResetDevice();
}

bool isDeviceLost()
{
	// Get the state of the graphics device.
	HRESULT hr = gd3dDevice->TestCooperativeLevel();

	// If the device is lost and cannot be reset yet then
	// sleep for a bit and we'll try again on the next 
	// message loop cycle.
	if (hr == D3DERR_DEVICELOST) {
		Sleep(20);
		return true;
	}
	// Driver error, exit.
	else if (hr == D3DERR_DRIVERINTERNALERROR) {
		MessageBox(0, "Internal Driver Error...Exiting", 0, 0);
		PostQuitMessage(0);
		return true;
	}
	// The device is lost but we can reset and restore it.
	else if (hr == D3DERR_DEVICENOTRESET) {
		onLostDevice();
		HR(gd3dDevice->Reset(&gd3dPP));
		onResetDevice();
		return false;
	}
	else {
		return false;
	}
}

LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	static bool minOrMaxed = false;

	RECT clientRect = { 0, 0, 0, 0 };

	switch (msg) {
	case WM_ACTIVATE:
		// capture the mouse when window is activated
		if (LOWORD(wParam) == WA_ACTIVE) {
			SetCapture(ghMainWnd);
		}

	case WM_SIZE:
		if (gd3dDevice) {
			gd3dPP.BackBufferWidth = LOWORD(lParam);
			gd3dPP.BackBufferHeight = HIWORD(lParam);

			if (wParam == SIZE_MINIMIZED) {
				gAppPaused = true;
				minOrMaxed = true;
			}
			else if (wParam == SIZE_MAXIMIZED) {
				gAppPaused = false;
				minOrMaxed = true;
				onLostDevice();
				HR(gd3dDevice->Reset(&gd3dPP));
				onResetDevice();
			}
			else if (wParam == SIZE_RESTORED) {
				gAppPaused = false;
				// Are we restoring from a mimimized or maximized state, 
				// and are in windowed mode?  Then reset device.
				if (minOrMaxed && gd3dPP.Windowed)
				{
					onLostDevice();
					HR(gd3dDevice->Reset(&gd3dPP));
					onResetDevice();
				}
				minOrMaxed = false;
			}
		}
		return 0;

	// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		GetClientRect(ghMainWnd, &clientRect);
		gd3dPP.BackBufferWidth = clientRect.right;
		gd3dPP.BackBufferHeight = clientRect.bottom;
		onLostDevice();
		HR(gd3dDevice->Reset(&gd3dPP));
		onResetDevice();
		return 0;

	case WM_CLOSE:
		DestroyWindow(ghMainWnd);
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_KEYDOWN:
		gKeyboard.HandleKeyDown(wParam);
		return 0;

	case WM_KEYUP:
		gKeyboard.HandleKeyUp(wParam);
		return 0;
	}

	return DefWindowProc(ghMainWnd, msg, wParam, lParam);
}

//______________________________________________________________________________
// WINDOWS CALLBACK
LRESULT CALLBACK
WinProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Don't start processing messages until the application has been created.
	if (gAppCreated)
		return msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}

//______________________________________________________________________________
// WINMAIN
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
                   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string winCaption = "My Little Program";
	D3DDEVTYPE devType = D3DDEVTYPE_HAL;
	DWORD requestedVP = D3DCREATE_HARDWARE_VERTEXPROCESSING;

	//_____________________________________________________________
	// 1. Initiate window
	WNDCLASS wc = {};
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WinProc;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.lpszClassName = "MyLittleProgramClass";
	if (!RegisterClass(&wc)) {
		MessageBox(0, "RegisterClass Failed", 0, 0);
		PostQuitMessage(0);
	}

	RECT WindowSize = { 0, 0, 1024, 768 };
	AdjustWindowRect(&WindowSize, WS_OVERLAPPEDWINDOW, false);
	ghMainWnd = CreateWindow("MyLittleProgramClass", "My Little Program",
		WS_OVERLAPPEDWINDOW, 100, 100, WindowSize.right, WindowSize.bottom,
		0, 0, hInstance, 0);
	if (!ghMainWnd) {
		MessageBox(0, "CreateWindow Failed", 0, 0);
		PostQuitMessage(0);
	}
	ShowWindow(ghMainWnd, SW_SHOW);
	UpdateWindow(ghMainWnd);

	//_____________________________________________________________
	// 2. Initiate DirectX
	gd3d = Direct3DCreate9(D3D_SDK_VERSION);
	if (!gd3d) {
		MessageBox(0, "Direct3DCreate9 Failed", 0, 0);
		PostQuitMessage(0);
	}
	// Verify hardware support in windowed and full screen modes.
	D3DDISPLAYMODE displayMode;
	gd3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode);
	HR(gd3d->CheckDeviceType(D3DADAPTER_DEFAULT, devType,
		displayMode.Format,	displayMode.Format, true));
	HR(gd3d->CheckDeviceType(D3DADAPTER_DEFAULT, devType,
		D3DFMT_X8R8G8B8, D3DFMT_X8R8G8B8, false));
	// Verify support for requested vertex processing and pure device.
	D3DCAPS9 deviceCaps;
	HR(gd3d->GetDeviceCaps(D3DADAPTER_DEFAULT, devType, &deviceCaps));
	DWORD deviceBehaviorFlags = 0;
	if (deviceCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) {
		deviceBehaviorFlags |= requestedVP;
	}
	else {
		deviceBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}
	// If pure device and HW T&L supported
	if (deviceCaps.DevCaps & D3DDEVCAPS_PUREDEVICE &&
		deviceBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING) {
		deviceBehaviorFlags |= D3DCREATE_PUREDEVICE;
	}

	// Create the device
	gd3dPP.BackBufferWidth = 0;
	gd3dPP.BackBufferHeight = 0;
	gd3dPP.BackBufferFormat = D3DFMT_UNKNOWN;
	gd3dPP.BackBufferCount = 1;
	gd3dPP.MultiSampleType = D3DMULTISAMPLE_8_SAMPLES;
	gd3dPP.MultiSampleQuality = 0;
	gd3dPP.SwapEffect = D3DSWAPEFFECT_DISCARD;
	gd3dPP.hDeviceWindow = ghMainWnd;
	gd3dPP.Windowed = true;
	gd3dPP.EnableAutoDepthStencil = true;
	gd3dPP.AutoDepthStencilFormat = D3DFMT_D24S8;
	gd3dPP.Flags = 0;
	gd3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	gd3dPP.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
	HR(gd3d->CreateDevice(D3DADAPTER_DEFAULT, devType, ghMainWnd,
		deviceBehaviorFlags, &gd3dPP, &gd3dDevice));

	// Check device caps
	D3DCAPS9 caps;
	HR(gd3dDevice->GetDeviceCaps(&caps));
	if (caps.VertexShaderVersion < D3DVS_VERSION(2, 0)) {
		MessageBox(0, "Vertex Shader version 2.0 required", 0, 0);
		PostQuitMessage(0);
	}
	if (caps.PixelShaderVersion < D3DPS_VERSION(2, 0)) {
		MessageBox(0, "Pixel Shader version 2.0 required", 0, 0);
		PostQuitMessage(0);
	}

	// Triangle culling
	//gd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	
	//_____________________________________________________________
	// 3. Initialize DirectInput
	HR(DirectInput8Create(hInstance, DIRECTINPUT_VERSION,
		IID_IDirectInput8, (void**)&gdinput, 0));
	MouseController mouseController(gdinput,
		DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	gMouse = &mouseController;  // Set a global pointer to the mouse

	//_____________________________________________________________
	// 4. Initialize Application

	initializeApp();
	gAppCreated = true;

	// Initialisation Complete!
	//*************************************************************
	//_____________________________________________________________
	// Run
	MSG msg;
	msg.message = WM_NULL;
	__int64 cntsPerSec = 0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt = 1.0f / (float)cntsPerSec;
	__int64 prevTimeStamp = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevTimeStamp);

	while (msg.message != WM_QUIT) {
		// If there are windows messages, process them...
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		// Otherwise, do animation/game stuff.
		else {
			// If the application is paused then free some CPU 
			// cycles to other applications and then continue on
			// to the next frame.
			if (gAppPaused)	{
				Sleep(20);
				continue;
			}
			else if (!isDeviceLost()) {
				__int64 currTimeStamp = 0;
				// Get time between iterations
				QueryPerformanceCounter((LARGE_INTEGER*)&currTimeStamp);
				float dt = (currTimeStamp - prevTimeStamp)*secsPerCnt;

				updateScene(dt);
				drawScene();

				// Prepare for next iteration
				prevTimeStamp = currTimeStamp;
			}
		}
	}

	// Finished running!
	//****************************************************************
	//________________________________________________________________
	// Begin shutdown procedures
	shutdownApp();
	ReleaseCOM(gd3d);
	ReleaseCOM(gd3dDevice);

	return msg.wParam;
}