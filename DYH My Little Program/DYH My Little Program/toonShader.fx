// Does basic ambient, diffuse, and specular lighting.
//=============================================================================


uniform extern float4x4 gWorld;
uniform extern float4x4 gWorldInverseTranspose;
uniform extern float4x4 gWVP;

uniform extern float3 gLightVecW;
uniform extern float3 gEyePosW;

uniform extern float4 gDiffuseMtrl;
uniform extern float4 gDiffuseLight;
uniform extern float4 gAmbientMtrl;
uniform extern float4 gAmbientLight;
uniform extern float4 gSpecularMtrl;
uniform extern float4 gSpecularLight;
uniform extern float gSpecularPower;

uniform extern float gTime;

struct OutputVS
{
	float4 posH : POSITION0;
	float3 posW : TEXCOORD0;
	float3 normalW : TEXCOORD1;
	float4 color : COLOR0;
};

OutputVS ToonShaderVS(float3 posL : POSITION0, float3 normalL : NORMAL0) 
{
	OutputVS outVS = (OutputVS)0;

	// Transform vertex to clip space and world space
	outVS.posH = mul(float4(posL, 1.0f), gWVP);
	outVS.posW = mul(float4(posL, 1.0f), gWorld).xyz;

	// Transform normal to world space
	float3 normalW = mul(float4(normalL, 0.0f), gWorldInverseTranspose).xyz;

	outVS.normalW = normalW;

	// Ambient
	float4 ambient = gAmbientMtrl * gAmbientLight;

	outVS.color.rgb = (ambient).xyz;
	outVS.color.a = gDiffuseMtrl.a;

	return outVS;
}

float4 ToonShaderPS(float3 posW : TEXCOORD0,
					float3 normalW : TEXCOORD1,
					float4 color : COLOR0) : COLOR0 {
	normalW = normalize(normalW);

	// Diffuse
	float diffuseFactor = max(dot(normalW, gLightVecW), 0.0f);
	if (diffuseFactor < 0.1f) {
		return (float4)0;
	}
	else if (diffuseFactor < 0.45f) {
		diffuseFactor = 0.4f;
	}
	else if (diffuseFactor < 0.85f) {
		diffuseFactor = 0.6f;
	}
	else {
		diffuseFactor = 1.0f;
	}
	float4 diffuse = diffuseFactor * gDiffuseMtrl * gDiffuseLight;

	// Specular
	float3 vertexToEye = normalize(gEyePosW - posW);
	float3 halfAngle = normalize(vertexToEye + gLightVecW);
	float specularFactor = pow(max(dot(normalW, halfAngle), 0), gSpecularPower);
	if (specularFactor > 0.98f) {
		specularFactor = 1.0f;
	}
	else {
		specularFactor = 0.0f;
	}
	float4 specular = specularFactor * gSpecularMtrl * gSpecularLight;

	color += diffuse + specular;

	return color;
}

technique ToonShaderTech
{
	pass POSITION0
	{
		vertexShader = compile vs_2_0 ToonShaderVS();
		pixelShader = compile ps_2_0 ToonShaderPS();
	}
}