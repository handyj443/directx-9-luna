#pragma once

#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }

#if defined(DEBUG) | defined(_DEBUG)
#ifndef HR
#define HR(x)                                      \
		{                                                  \
		HRESULT hr = x;                                \
		if(FAILED(hr))                                 \
			{                                              \
                OutputDebugString("Something failed");     \
			}                                              \
		}
#endif

#else
#ifndef HR
#define HR(x) x;
#endif
#endif