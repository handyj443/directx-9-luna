#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <Windows.h>
#include "InputControllers.h"
#include "macros.h"

extern HWND ghMainWnd;

//___________________________________________________________________
// Keyboard
void KeyboardController::HandleKeyDown(WPARAM wParam) {
	if (wParam == VK_ESCAPE) esc = true;
	else if (wParam == VK_SPACE) space = true;
	else if (wParam == 'F') f = true;
	else if (wParam == 'W') w = true;
	else if (wParam == 'S') s = true;
	else if (wParam == 'A') a = true;
	else if (wParam == 'D') d = true;
}

void KeyboardController::HandleKeyUp(WPARAM wParam) {
	if (wParam == VK_ESCAPE) esc = false;
	else if (wParam == VK_SPACE) space = false;
	else if (wParam == 'F') f = false;
	else if (wParam == 'W') w = false;
	else if (wParam == 'S') s = false;
	else if (wParam == 'A') a = false;
	else if (wParam == 'D') d = false;
}

//___________________________________________________________________
// Mouse
MouseController::MouseController(IDirectInput8 *di, DWORD mouseCoopFlags) {
	mdinput = di;
	ZeroMemory(&mMouse, sizeof(mMouse));
	HR(di->CreateDevice(GUID_SysMouse, &mMouse, 0));
	HR(mMouse->SetDataFormat(&c_dfDIMouse2));
	HR(mMouse->SetCooperativeLevel(ghMainWnd, mouseCoopFlags));
	HR(mMouse->Acquire());
}

MouseController::~MouseController() {
	mMouse->Unacquire();
	ReleaseCOM(mMouse);
}

void MouseController::poll() {
	HRESULT hr = mMouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&mMouseState);
	if (FAILED(hr))
	{
		// Mouse lost, zero out mouse data structure.
		ZeroMemory(&mMouseState, sizeof(mMouseState));
		// Try to acquire for next time we poll.
		hr = mMouse->Acquire();
	}
}
bool MouseController::leftdown() {
	return (mMouseState.rgbButtons[0] & 0x80) != 0;
}

bool MouseController::middledown() {
	return (mMouseState.rgbButtons[2] & 0x80) != 0;
}

bool MouseController::rightdown() {
	return (mMouseState.rgbButtons[1] & 0x80) != 0;
}

float MouseController::dx() {
	return (float)mMouseState.lX;
}

float MouseController::dy() {
	return (float)mMouseState.lY;
}

float MouseController::dz() {
	return (float)mMouseState.lZ;
}