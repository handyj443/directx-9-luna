#include <cstdio>

void Exercise1_2()
{
	FILE *fMultisample;
	fopen_s(&fMultisample, _T("Multisample Support.txt"), _T("w"));

	// Windowed 
	// D3DFMT_X8R8G8B8
	fprintf(fMultisample, _T("Multisample test\n\n"
		"Windowed mode\nD3DFMT_X8R8G8B8\n\n"));
	for (int i = 0; i <= 16; ++i) {
		DWORD qualityLevels;
		HRESULT MultiSampleSupported;
		MultiSampleSupported = gd3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, true, (D3DMULTISAMPLE_TYPE)i, &qualityLevels);
		if (MultiSampleSupported == D3D_OK) {
			fprintf(fMultisample, _T("Multisampling Samples: %d\t"
				"Quality Levels: %d\n"), i, qualityLevels);
		}
	}

	// D3DFMT_R5G6B5
	fprintf(fMultisample, _T("\nMultisample test\n\n"
		"Windowed mode\nD3DFMT_R5G6B5\n\n"));
	for (int i = 0; i <= 16; ++i) {
		DWORD qualityLevels;
		HRESULT MultiSampleSupported;
		MultiSampleSupported = gd3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_R5G6B5, true, (D3DMULTISAMPLE_TYPE)i, &qualityLevels);
		if (MultiSampleSupported == D3D_OK) {
			fprintf(fMultisample, _T("Multisampling Samples: %d\t"
				"Quality Levels: %d\n"), i, qualityLevels);
		}
	}

	// Full Screen
	// D3DFMT_X8R8G8B8
	fprintf(fMultisample, _T("\nMultisample test\n\n"
		"Full screen mode\nD3DFMT_X8R8G8B8\n\n"));
	for (int i = 0; i <= 16; ++i) {
		DWORD qualityLevels;
		HRESULT MultiSampleSupported;
		MultiSampleSupported = gd3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_X8R8G8B8, false, (D3DMULTISAMPLE_TYPE)i, &qualityLevels);
		if (MultiSampleSupported == D3D_OK) {
			fprintf(fMultisample, _T("Multisampling Samples: %d\t"
				"Quality Levels: %d\n"), i, qualityLevels);
		}
	}

	// D3DFMT_R5G6B5
	fprintf(fMultisample, _T("\nMultisample test\n\n"
		"Full screen mode\nD3DFMT_R5G6B5\n\n"));
	for (int i = 0; i <= 16; ++i) {
		DWORD qualityLevels;
		HRESULT MultiSampleSupported;
		MultiSampleSupported = gd3d->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_R5G6B5, false, (D3DMULTISAMPLE_TYPE)i, &qualityLevels);
		if (MultiSampleSupported == D3D_OK) {
			fprintf(fMultisample, _T("Multisampling Samples: %d\t"
				"Quality Levels: %d\n"), i, qualityLevels);
		}
	}

	fclose(fMultisample);
}

void Exercise1_5()
{
	FILE *fAdaptorCaps;
	fopen_s(&fAdaptorCaps, _T("Adaptor Capabilitiies.txt"), _T("w"));
	D3DCAPS9 deviceCaps;
	HR(gd3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &deviceCaps));
	float MaxPointSize = deviceCaps.MaxPointSize;
	fprintf(fAdaptorCaps, "MaxPointSize: %f\n", MaxPointSize);

	//uint MaxPrimitiveCount = deviceCaps.MaxPrimitiveCount;
	//uint MaxActiveLights = deviceCaps.MaxActiveLights;
	//uint MaxUserCliPlanes = deviceCaps.MaxUserClipPlanes;
	//uint MaxVertexIndex = deviceCaps.MaxVertexIndex;
	//uint MaxVertexShaderConst = deviceCaps.MaxVertexShaderConst;

#define LIST_OF_UINT_CAPS \
	X(MaxPrimitiveCount) \
	X(MaxActiveLights) \
	X(MaxUserClipPlanes) \
	X(MaxVertexIndex) \
	X(MaxVertexShaderConst)

#define X(name) UINT name = deviceCaps.##name; \
	fprintf(fAdaptorCaps, #name ": %d\n", name);
	LIST_OF_UINT_CAPS
#undef X

}